class LibrarianActionsController < ApplicationController
  before_action :authenticate_librarian!
  def index
    @librarians = Librarian.all
  end

  def new
  end
end
