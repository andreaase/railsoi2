class AddLibrarianToReservation < ActiveRecord::Migration[5.1]
  def change
    add_reference :reservations, :librarian, foreign_key: true
  end
end
