class CreateReservations < ActiveRecord::Migration[5.1]
  def change
    create_table :reservations do |t|
      t.references :book, foreign_key: true
      t.references :client, foreign_key: true
      
      t.boolean :status

      t.timestamps
    end
  end
end
