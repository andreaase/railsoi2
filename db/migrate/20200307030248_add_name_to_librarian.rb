class AddNameToLibrarian < ActiveRecord::Migration[5.1]
  def change
    add_column :librarians, :name, :string
  end
end
