Rails.application.routes.draw do
  get 'librarian_actions/index'

  get 'librarian_actions/new'

  devise_for :librarians
  get 'home/index'

  root 'home#index'

  resources :reservations
  resources :clients
  resources :books
  resources :authors

  get '/novo_bibliotecario', to: 'librarian_actions#index', as: 'librarians'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
