require 'test_helper'

class LibrarianActionsControllerTest < ActionDispatch::IntegrationTest
  test "should get index" do
    get librarian_actions_index_url
    assert_response :success
  end

  test "should get new" do
    get librarian_actions_new_url
    assert_response :success
  end

end
